Giới thiệu
==========

![Hình chụp chương trình](hocdanh.png)

Đây là chương trình tập luyện gõ bàn phím cho tiếng Việt. Chương trình này do bạn Trần Ngọc Tú viết với sự hướng dẫn của thầy Bửu Đông, Huế. Bạn Tú hoàn thành chương trình này năm 2000. Nếu các bạn biết bạn Tú và thầy Đông, xin vui lòng nhờ bạn Tú hay thầy Đông liên hệ địa chỉ email hailangvn@mocdiep.com.

Nếu các bạn có thể thêm các lệnh để cài đặt chương trình này trên các hệ điều hành khác, vui lòng gửi các lệnh đó đến địa chỉ mail hailangvn@mocdiep.com để giúp cho các bạn khác dễ dàng sử dụng chương trình này.

Cài đặt
=======

* Với các máy chạy Ubuntu/Debian:

    1. Mở cửa sổ dòng lệnh terminal (Ubuntu: mở danh sách ứng dụng, gõ term và bấm Enter)
    2. Chép và dán các lệnh sau vào cửa sổ dòng lệnh

```bash
wget -c https://gitlab.com/mocdiep/tapluyen/-/tree/master/HD/hocdanh.zip &&
unzip hocdanh.zip -d /tmp/ &&
pushd /tmp/HD && sudo bash install; popd
```

Sử dụng
=======

* Với các máy chạy Ubuntu/Debian:

    1. Mở cửa sổ dòng lệnh terminal (Ubuntu: mở danh sách ứng dụng, gõ term và bấm Enter)
    2. Bấm lệnh `hocdanh` và nhấn Enter

Gỡ bỏ
=====

* Với các máy chạy Ubuntu/Debian:

    1. Mở cửa sổ dòng lệnh terminal (Ubuntu: mở danh sách ứng dụng, gõ term và bấm Enter)
    2. Chép và dán các lệnh sau vào cửa sổ dòng lệnh, nếu bạn muốn gỡ bỏ dosbox thì trả lời y, n nếu muốn giữ lại

```bash
pushd /opt/HD && sudo bash uninstall; popd
```

Các nguồn khác
==============

* [ecosia.org> tập gõ bàn phím]
* [taimienphi.vn> tập gõ bàn phím 10 ngón]
* [typingstudy.com]

[ecosia.org> tập gõ bàn phím]: https://www.ecosia.org/search?q=t%E1%BA%ADp+g%C3%B5+b%C3%A0n+ph%C3%ADm&addon=firefox&addonversion=4.0.4
[taimienphi.vn> tập gõ bàn phím 10 ngón]: https://taimienphi.vn/tk/t%E1%BA%ADp+g%C3%B5+b%C3%A0n+ph%C3%ADm+10+ng%C3%B3n
[typingstudy.com]: https://www.typingstudy.com/vi-vietnamese-3/
